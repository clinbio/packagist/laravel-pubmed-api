<p align="center"><img src="https://banners.beyondco.de/Laravel%20Pubmed%20API.png?theme=light&packageManager=composer+require&packageName=dterumal%2Flaravel-pubmed-api&pattern=architect&style=style_1&description=PubMed+API+Service+for+Laravel&md=1&showWatermark=0&fontSize=100px&images=book-open"></p>

## Installation

You can install the package via composer:

```bash
composer require dterumal/laravel-pubmed-api
```

You can publish the config file with:
```bash
php artisan vendor:publish --provider="Dterumal\LaravelPubmedApi\LaravelPubmedApiServiceProvider" --tag="laravel-pubmed-api-config"
```

This is the contents of the published config file:

```php
return [

    /*
    |--------------------------------------------------------------------------
    | PubMed Settings
    |--------------------------------------------------------------------------
    |
    | Make sure to use a proper URI to access PubMed API service.
    |
    */
    'uri' => env('PUBMED_URI', 'https://eutils.ncbi.nlm.nih.gov'),

    'path' => env('PUBMED_PATH', '/entrez/eutils/esummary.fcgi'),

    'response_type' => env('PUBMED_RESPONSE_TYPE', 'json'),

    'database' => env('PUBMED_DB', 'pubmed'),

    'timeout' => 2.0,

    /*
    |--------------------------------------------------------------------------
    | Cache Settings
    |--------------------------------------------------------------------------
    |
    | Define the max age (or TTL) to cache responses. This will speed up your
    | requests.
    |
    */
    'cache_ttl' => env('PUBMED_CACHE_TTL', true),
];
```

## Usage

```php
use Dterumal\LaravelPubmedApi\LaravelPubmedApi;

app(LaravelPubmedApi::class)->find('12345');
```

## Testing

```bash
composer test
```

## Changelog

Please see [CHANGELOG](https://gitlab.sib.swiss/clinbio/packagist/laravel-pubmed-api/-/releases) for more information on what has changed recently.

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) for details.

## Security Vulnerabilities

Please review [our security policy](.gitlab/SECURITY.md) on how to report security vulnerabilities.

## Credits

- [Dillenn Terumalai](https://gitlab.sib.swiss/dterumal)

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.
