<?php
// config for Dterumal/LaravelPubmedApi
return [

    /*
    |--------------------------------------------------------------------------
    | PubMed Settings
    |--------------------------------------------------------------------------
    |
    | Make sure to use a proper URI to access PubMed API service.
    |
    */
    'uri' => env('PUBMED_URI', 'https://eutils.ncbi.nlm.nih.gov'),

    'path' => env('PUBMED_PATH', '/entrez/eutils/esummary.fcgi'),

    'response_type' => env('PUBMED_RESPONSE_TYPE', 'json'),

    'database' => env('PUBMED_DB', 'pubmed'),

    'timeout' => 2.0,

    /*
    |--------------------------------------------------------------------------
    | Cache Settings
    |--------------------------------------------------------------------------
    |
    | Define the max age (or TTL) to cache responses. This will speed up your
    | requests.
    |
    */
    'cache_ttl' => env('PUBMED_CACHE_TTL', true),
];
