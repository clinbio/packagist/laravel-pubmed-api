<?php

use Dterumal\LaravelPubmedApi\LaravelPubmedApi;

it('fails with an invalid id', function () {
    $this->expectException(Exception::class);
    $this->expectExceptionMessage('Invalid uid x at position=0');
    app(LaravelPubmedApi::class)->find('x', false);

});

it('works with valid id', function () {
    $response = app(LaravelPubmedApi::class)->find('1', false);
    expect($response)->toBeString();
});

it('can decode the response as json', function () {
    $response = app(LaravelPubmedApi::class)->find('1', true);
    expect($response)->toBeArray();
});

it('can decode the response as xml', function () {
    config()->set('pubmed-api.response_type', 'xml');
    $response = app(LaravelPubmedApi::class)->find('1', true);
    expect($response)->toBeObject();
});
