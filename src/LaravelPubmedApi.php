<?php

namespace Dterumal\LaravelPubmedApi;

use Brightfish\CachingGuzzle\Client;
use Dterumal\LaravelPubmedApi\Contracts\PubmedApiInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Utils;
use Illuminate\Support\Str;
use Psr\SimpleCache\CacheInterface as Cache;

class LaravelPubmedApi implements PubmedApiInterface
{
    /**
     * @var Cache
     */
    protected Cache $cache;

    /**
     * @var Client
     */
    protected Client $client;

    /**
     * PubMedApi constructor
     */
    public function __construct(Cache $cache)
    {
        $this->cache = $cache;

        $this->client = new Client($this->cache, [
            'cache_ttl' => config('pubmed-api.cache_ttl'),
            'cache_log' => false,
            'base_uri' => config('pubmed-api.uri'),
            'timeout' => config('pubmed-api.timeout.')
        ]);
    }

    /**
     * Returns a PubMed publication metadata
     *
     * @param  mixed  $id
     * @return string
     * @throws \Exception|\GuzzleHttp\Exception\GuzzleException
     */
    public function find($id, $decode = true)
    {
        try {
            $response = $this->client->request('GET', config('pubmed-api.path'), [
                'query' => [
                    'db' => 'pubmed',
                    'id' => $id,
                    'retmode' => config('pubmed-api.response_type')
                ]
            ]);
        } catch (RequestException $e) {
            throw new \Exception($e);
        } finally {
            $content = $response->getBody()->getContents();

            if(Str::of($content)->contains('error')) {
                if (config('pubmed-api.response_type') === 'json') {
                    $content = Utils::jsonDecode($content, true);
                }

                if (config('pubmed-api.response_type') === 'xml') {
                    $content = simplexml_load_string($content);
                }

                throw new \Exception($content['error']);
            }

            if ($decode) {
                if (config('pubmed-api.response_type') === 'json') {
                    return Utils::jsonDecode($content, true);
                }

                if (config('pubmed-api.response_type') === 'xml') {
                    return simplexml_load_string($content);
                }
            }


            return $content;
        }
    }
}
