<?php

namespace Dterumal\LaravelPubmedApi\Contracts;

interface PubmedApiInterface
{
    /**
     * Returns a PubMed publication metadata
     *
     * @param  int $id
     * @return mixed
     */
    public function find(int $id);
}
