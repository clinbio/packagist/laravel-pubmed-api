<?php

namespace Dterumal\LaravelPubmedApi;

use Illuminate\Support\Facades\Facade;

/**
 * @see \Dterumal\LaravelPubmedApi\LaravelPubmedApi
 */
class LaravelPubmedApiFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'laravel-pubmed-api';
    }
}
